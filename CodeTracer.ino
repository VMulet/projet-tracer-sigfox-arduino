#include <SigFox.h>//declaration of SigFox librairies needed to send message with SigFox
#include <ArduinoLowPower.h>//Useful to switchoff the board
#define TIMEOFF 1*60*1000// set the delay to 1 minutes

int debug = false;
char buf[12]= "";
char buff[4];
int tmp=2;
int minutes=15;// set the delay to 15 minutes ( cf "for" below );

void setup() 
{
  if (!SigFox.begin())
  {
    reboot(); //try rebooting if something is wrong
  }
  SigFox.end();// Send module to standby until we need to send a message
  if (debug) {
    SigFox.debug();// Enable debugging. Enabling the debugging all the power saving features are disabled and the led indicated as signaling pin (LED_BUILTIN as default) is used during transmission and receive events.
    Serial.begin(9600);
    while(!Serial){}; // wait for serial port to connect. Needed for native USB
  }
  delay(10000);
}
void reboot()
{
  NVIC_SystemReset();
  while(1);
}
void cleararray(char arr[])
{
  for(int i=0; i<sizeof(arr);i++)
  {
    arr[i]=0;
  }
}

char* autonomie(char* buff)
{
  //float reference[]={}; //check power needed by the device and check with data shown here: http://www.jmargolin.com/furnace/Ultra-Power_AA_MX1500.pdf 
  float vmin=2.5;
  int voltage=analogRead(A1);
  float vin=(voltage*3.2)/1024.0;//analogRead return a int beetween 0 and 1023 ( 1023=Circuit Operating Voltage (3.3V) ).
  int autonomie=int(100*(vin-vmin)/(3.2-vmin));
  if (autonomie==100){autonomie--;}
  itoa(autonomie,buff,10);
  return buff;
  /*for(int i=1; i<sizeof(reference);i++)
  {
    if (voltage>reference[i-1] and voltage<reference[i])
    {
      autonomie=(100-(10*i))/10;
      return char(autonomie);
    }
  }*/
}

void loop() 
{
    autonomie(buff);
    for(int j=0; j<3;j++){buf[j]=buff[j];}
    if(!SigFox.begin())
    {
      buf[tmp++]='1';
      if(tmp>=11){tmp=2;}
      return;
    }
    delay(100);//need to wait after the first configuration ( 100ms ) 
    SigFox.beginPacket();
    SigFox.print(buf);
    if(SigFox.endPacket())
    {
      buf[tmp++]='2';
      if(tmp>=11){tmp=2;}
      return;
    }
    SigFox.end();
    tmp=2;
    cleararray(buf);
    for(int i=0;i<minutes;i++){ LowPower.sleep(TIMEOFF);}
}


